package main

import (
	"github.com/rs/zerolog/log"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
	"os"
)

func testMatrixCredentials() {
	client, err := mautrix.NewClient(conf["matrix_home"], "", "")
	if err !=  nil {
		log.Error().Err(err).Send()
	}

	_, err = client.Login(&mautrix.ReqLogin{
		Type: mautrix.AuthTypePassword,
		Identifier: mautrix.UserIdentifier{Type: mautrix.IdentifierTypeUser, User: conf["matrix_user"]},
		Password: conf["matrix_pass"],
		StoreCredentials: true,
	})
	if err != nil {
		log.Error().Err(err).Send()
		log.Error().Msg("Failed to test login for matrix")
		os.Exit(3)
	}

	me, err := client.Whoami()
	if err != nil {
		log.Error().Err(err).Send()
	}
	log.Debug().Str("User-ID", me.UserID.String()).Msg("Login")

	rooms, err := client.JoinedRooms()
	log.Debug().Interface("Joined Rooms", rooms.JoinedRooms).Send()

	if inRoom(rooms.JoinedRooms) {
		log.Debug().Str("Room-ID", conf["matrix_chat"]).Msg("Bot is in the right room")
	} else {
		log.Error().Str("Room-ID", conf["matrix_chat"]).Msg("The Bot is not in the right Matrix-Room. Add it and check again")
		os.Exit(3)
	}


	client.Logout()
}

func inRoom(rooms []id.RoomID) bool {
	for _, r := range rooms {
		if r.String() == conf["matrix_chat"] {
			return true
		}
	}
	return false
}