# Softwerke-Bot

Ein Matrix-Bot zur Erleichterung der Plenumsorganisation.

## Funktionen

- [ ] Benachrichtigung für das nächste Plenum schicken
- [ ] Themenvorschläge können per Command gespeichert werden
- [ ] Den Raum (online Meeting) in der Erinnerung verlinken
- [ ] Ein Pad für das neue Plenum anlegen aus einer Vorlage
- [ ] Das aktuelle Pad in den Erinnerungen verlinken
- [ ] Zugriff auf vergangene Pads verlinken
- [ ] Pad in Kalendertermin hinzufügen

Weitere Wünsche können gerne geäußert werden :)