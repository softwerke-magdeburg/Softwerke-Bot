module softwerke-bot

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/rs/zerolog v1.25.0
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	maunium.net/go/mautrix v0.9.29 // indirect
)
