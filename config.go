package main

import (
	"github.com/rs/zerolog/log"
	"os"
)

var conf = map[string]string{}

func loadEnv() {
	calCredentials()
	matrixCredentials()
}

func matrixCredentials() {
	conf["matrix_home"] = os.Getenv("MATRIX_HOMESERVER")
	conf["matrix_user"] = os.Getenv("MATRIX_USER")
	conf["matrix_pass"] = os.Getenv("MATRIX_PASS")
	conf["matrix_chat"] = os.Getenv("MATRIX_CHAT")
	if conf["matrix_chat"] == "" || conf["matrix_pass"] == "" || conf["matrix_user"] == "" || conf["matrix_home"] == "" {
		log.Error().Str("Homeserver", conf["matrix_home"]).Str("Chat-ID", conf["matrix_chat"]).Str("User", conf["matrix_user"]).Str("Password", conf["matrix_pass"]).Msg("Something for matrix is not provided, check again")
		os.Exit(3)
	}
	testMatrixCredentials()
}

func calCredentials() {
	url := os.Getenv("CAL_URL")
	if url == "" {
		log.Error().Msg("Calendar URL is not defined!")
		os.Exit(4)
	}
	conf["cal_url"] = url
	conf["cal_user"] = os.Getenv("CAL_USER")
	conf["cal_pass"] = os.Getenv("CAL_PASS")
}