package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

const version = "v0.1"

func main() {
	setupLogging()
	log.Info().Str("Version", version).Msg("Starting Softwerke Bot")
	zonename, _ := time.Now().Zone()
	log.Debug().Str("Zeitzone", zonename).Msg("The Timezone is set to this value")
	loadEnv()
}

func setupLogging() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "02.01.06 15:04"})
	if _, ok := os.LookupEnv("DEBUG"); ok {
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
		log.Debug().Msg("Debugging enabled")
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
}